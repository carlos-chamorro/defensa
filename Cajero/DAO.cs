﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Cajero
{
    class DAO
    {
        private FileStream fs;
        private BinaryWriter bw;
        private BinaryReader br;
        private int n = 0;
        private int tam = 40 + 40 + 40 + 40 + 40 + 40 + 40 + 40;

        public DAO(String filename)
        {
            if (File.Exists(filename))
            {
                fs = new FileStream(filename, FileMode.Open, FileAccess.ReadWrite);
                bw = new BinaryWriter(fs);
                br = new BinaryReader(fs);
            }
            else
            {
                fs = new FileStream(filename, FileMode.Create, FileAccess.ReadWrite);
                bw = new BinaryWriter(fs);
                br = new BinaryReader(fs);
                bw.BaseStream.Seek(0, SeekOrigin.Begin);
                bw.Write(0);
            }
        }

    
        public string myTrim(string cadena)
        {
            string cad = "";
            char[] c = cadena.ToCharArray();

            for (int i = 0; i < cadena.Length; i++)
            {
                if (Char.IsLetterOrDigit(cadena, i) || Char.IsPunctuation(cadena, i))
                {
                    cad += c[i];
                }
            }

            return cad;
        }


     

        public bool Update(Archivo a)
        {
            try
            {
                bw.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = a.Id - 1;
                int posi = 4 + n * tam;
                bw.Seek(posi, SeekOrigin.Begin);
                bw.Write(n + 1);
                bw.Write(a.Nombre);
                bw.Write(a.Apellido);
                bw.Write(a.Cedula);
                bw.Write(a.Cuenta);
                bw.Write(a.Pin);
                bw.Write(a.Cordoba);
                bw.Write(a.Saldo);

               
                return true;
            }
            catch (Exception exe)
            {
                return false;
            }

        }
        public bool Save(Archivo a)
        {
            try
            {
                bw.Seek(0, SeekOrigin.Begin);
                
                int n = br.ReadInt32();
                int pos = 4 + n * tam;
                bw.Seek(pos,SeekOrigin.Begin);
                bw.Write(n + 1);
                bw.Write(a.Nombre);
                bw.Write(a.Apellido);
                bw.Write(a.Cedula);
                bw.Write(a.Cuenta);
                bw.Write(a.Pin);
                bw.Write(a.Cordoba);
                bw.Write(a.Saldo);

                bw.BaseStream.Seek(0, SeekOrigin.Begin);
                bw.Write(++n);
                
                return true;
            }
            catch (Exception exe)
            {
                return false;
            }
        }
        public bool erase(int cuentabor)
        {
            try
            {
                Archivo a = new Archivo();
                Archivo c = new Archivo();
                a = Findcuenta(cuentabor);
                int n = 9;


                return true;
            }
            catch (Exception exe)
            {
                return false;
            }

        }

            public Archivo Findcuenta(int cuenta)
        {
            Archivo a = new Archivo();
            bw.Seek(0, SeekOrigin.Begin);
            int n = br.ReadInt32();
            for(int i = 1;i <= n;i++){
                a = Findid(i);
                if(a.Cuenta.Equals(cuenta)){
                return a;
                }

            }    
                
        return a;
            }
        
        public Archivo Findid(int id){
        Archivo a = new Archivo();
            bw.Seek(0, SeekOrigin.Begin);
            n = br.ReadInt32();

            if (id <= 0 || id > n)
            {
                return null;
            }

            int pos = 4 + (id - 1) * tam;
            bw.Seek(pos, SeekOrigin.Begin);

            a.Id = br.ReadInt32();
            a.Nombre = br.ReadString();
            a.Apellido = br.ReadString();
            a.Cedula = br.ReadString();
            a.Cuenta = br.ReadInt32();
            a.Pin = br.ReadInt32();
            a.Cordoba = br.ReadBoolean();
            a.Saldo = br.ReadDouble();
            return a;
        }

        public int tamar() {
            bw.Seek(0, SeekOrigin.Begin);
            return br.ReadInt32();
        }
        public void close()
        {
            if (bw != null) { bw.Close(); }
            if (br != null) { br.Close(); }
            if (fs != null) { fs.Close(); }
        }
        
    }
}
