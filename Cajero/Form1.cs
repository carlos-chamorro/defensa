﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Cajero
{
    public partial class Form1 : Form
    {

        int psn = 0;
        
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox1.Enabled = true;
            tabControl1.Enabled = false;
            button1.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String password = "Admin1234";
            String textb = textBox1.Text;
            if(textb.Equals(password)){
                button1.Enabled = false;
                tabControl1.Enabled = true;
                textBox1.Enabled = false;
            }
            else{
                MessageBox.Show("Error en la contraseña","Cajero");
            }

            button2.Enabled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DAO cdao = new DAO("cajero.txt");
            Archivo c = new Archivo();
            
            String nombre="";
            String apellidos="";
            String ced = "";
            int ping = 0;
            int cuenta = 0;
            Double saldo = 0;
            Boolean cordoba = true;

            if (txtcedulanuevo.Text.Equals(""))
            {
                MessageBox.Show("Escriba la cedula");
                txtcedulanuevo.Focus();
            }
            else {
                ced = txtcedulanuevo.Text;
            }
            if (txtapellidosnuevo.Text.Equals(""))
            {
                MessageBox.Show("Escriba el Apellido del cliente");
                txtapellidosnuevo.Focus();
            }
            else {
                apellidos=  txtapellidosnuevo.Text;
            }
            if (txtnombrenuevo.Text.Equals(""))
            {
                MessageBox.Show("Escriba el nombre del cliente");
                txtnombrenuevo.Focus();
            }
            else {
                nombre = txtnombrenuevo.Text;
            }
            if (comboBox1.Text.Equals("Dollares"))
             {
                 cordoba = false;
             }
            if (txtsaldonuevo.Text.Equals(""))
            {
                MessageBox.Show("Escriba el saldo");
                txtsaldonuevo.Focus();
            }
            else {
                saldo = double.Parse(txtsaldonuevo.Text);
            }

            if (txtpingnuevo.Text.Equals(""))
            {
                MessageBox.Show("Escriba el ping");
                txtpingnuevo.Focus();
            }
            else {
                ping = int.Parse(txtpingnuevo.Text);            }
            if (txtcuentanueva.Text.Equals(""))
            {
                MessageBox.Show("Escriba la nueva cuenta");
                txtcuentanueva.Focus();
            }
            else
            {

                Archivo a = new Archivo();
                int cue = int.Parse(txtcuentanueva.Text);
                a = cdao.Findcuenta(cue);
                if (a.Cuenta != cue)
                {
                    cuenta = int.Parse(txtcuentanueva.Text);


                    c.Nombre = nombre;
                    c.Apellido = apellidos;
                    c.Cedula = ced;
                    c.Cuenta = cuenta;
                    c.Pin = ping;
                    c.Saldo = saldo;
                    c.Cordoba = cordoba;
                    
                    if (cdao.Save(c))
                    {
                        cdao.close();
                        this.Limpiar();
                    }
                    else
                    {
                        cdao.close();
                        MessageBox.Show(this,"llene los campos correctamente");
                    }
                }
                else 
                {
                    cdao.close();
                    MessageBox.Show(this, "Error ya existe esa cuenta");
                }
            }
            
        }

        private void Limpiar()
        {
            this.txtapellidosnuevo.Text = "";
            this.txtsaldonuevo.Text = "";
            this.txtcuentanueva.Text = "";
            this.txtpingnuevo.Text = "";
            this.txtcedulanuevo.Text = "";
            this.txtnombrenuevo.Text = "";
            this.comboBox1.Text = "Cordobas";
            psn = 0;

         
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Archivo a = new Archivo();
            DAO cdao = new DAO("cajero.txt");
            button2.Enabled = false;
            
            comboBox2.Enabled = false;
            txtpingup.Enabled = false;
            txtsaldoup.Enabled = false;
            txtnombreup.Enabled = false;
            txtapellidosup.Enabled = false;
            txtcedulaup.Enabled = false;
            button5.Enabled = false;
            button6.Enabled = false;
            comboBox1.Text = "Cordobas";
            for (int i = 1; i <= cdao.tamar(); i++)
            {
                a = cdao.Findid(i);
                String moneda = "";
                String mon = "";
                if (a.Cordoba == true)
                {
                    mon = "C$ ";
                    moneda = "Cordobas";
                }
                else
                {
                    mon = "$ ";
                    moneda = "Dollares";
                }
                table.Rows.Add(a.Cuenta, a.Nombre + " " + a.Apellido, a.Cedula, moneda, mon + a.Saldo);

            }
            

            cdao.close();
        }

        private void txtcuentanueva_KeyPress(object sender, KeyPressEventArgs e)
        {
            char c = e.KeyChar;

            if (Char.IsLetter(c) || Char.IsPunctuation(c) || Char.IsSeparator(c) || e.KeyChar == '$' || e.KeyChar == '=' || e.KeyChar == '+' || e.KeyChar == '´')
            {
                e.Handled = true;
            }
        }

        private void txtpingnuevo_KeyPress(object sender, KeyPressEventArgs e)
        {
            char c = e.KeyChar;

            if (Char.IsLetter(c) || Char.IsPunctuation(c) || Char.IsSeparator(c) || e.KeyChar == '$' || e.KeyChar == '=' || e.KeyChar == '+' || e.KeyChar == '´')
            {
                e.Handled = true;
            }
        }

        private void txtsaldonuevo_KeyPress(object sender, KeyPressEventArgs e)
        {
            char c = e.KeyChar;

            if (Char.IsLetter(c) || Char.IsSeparator(c) || e.KeyChar == '$' || e.KeyChar == '=' || e.KeyChar == '+' || e.KeyChar == '´' || e.KeyChar == ',' || e.KeyChar == ';' || e.KeyChar == ':')
            {
                e.Handled = true;
            }
            if (e.KeyChar == '.') {
                psn++;
                if (psn > 1) { 
                e.Handled = true;
                }
            
            }
        }

        private void txtnombrenuevo_KeyPress(object sender, KeyPressEventArgs e)
        {
            char c = e.KeyChar;

            if (char.IsNumber(c)|| Char.IsPunctuation(c)||Char.IsSeparator(c) || e.KeyChar == '$' || e.KeyChar == '=' || e.KeyChar == '+' || e.KeyChar == '´') {
                e.Handled = true;
            }
        }

        private void txtapellidosnuevo_KeyPress(object sender, KeyPressEventArgs e)
        {
            char c = e.KeyChar;

            if (char.IsNumber(c) || Char.IsPunctuation(c) || Char.IsSeparator(c) || e.KeyChar == '$' || e.KeyChar == '=' || e.KeyChar == '+' || e.KeyChar == '´')
            {
                e.Handled = true;
            }

            
        }

        private void txtcedulanuevo_KeyPress(object sender, KeyPressEventArgs e)
        {
            char c = e.KeyChar;

            if ( e.KeyChar == ',' || e.KeyChar == ';' || e.KeyChar == ':' || Char.IsSeparator(c) || e.KeyChar == '$' || e.KeyChar == '=' || e.KeyChar == '+' || e.KeyChar == '´')
            {
                e.Handled = true;
            }
           
        }

        private void tabPage3_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Limpiar();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {            

            if(e.KeyChar == Convert.ToChar(Keys.Enter)){
                this.button1_Click(sender,e);
            }
        }

        private void txtcuentaup_KeyPress(object sender, KeyPressEventArgs e)
        {

            char c = e.KeyChar;

            if (Char.IsLetter(c) || Char.IsPunctuation(c) || Char.IsSeparator(c) || e.KeyChar == '$' || e.KeyChar == '=' || e.KeyChar == '+' || e.KeyChar == '´')
            {
                e.Handled = true;
            }
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                DAO cdao = new DAO("cajero.txt");
                Archivo a = new Archivo();

                int cue = int.Parse(txtcuentaup.Text);
                a = cdao.Findcuenta(cue);
                if (a.Cuenta == cue)
                {
                    int id = a.Id;
                    String nombre = a.Nombre;
                    String apellidos = a.Apellido;
                    String ced = a.Cedula;
                    int ping = a.Pin;
                    int cuenta = int.Parse(txtcuentaup.Text);
                    Double saldo = a.Saldo;
                    Boolean cordoba = a.Cordoba;

                    txtpingup.Text = ping.ToString();
                    txtsaldoup.Text = saldo.ToString();
                    if (cordoba == true)
                    {
                        comboBox2.Text = "Cordobas";
                    }
                    else {
                        comboBox2.Text = "Dollares";
                    }
                    txtnombreup.Text = nombre;
                    txtapellidosup.Text = apellidos;
                    txtcedulaup.Text = ced;
                    txtcuentaup.Enabled = false;
                    comboBox2.Enabled = false;
                    txtpingup.Enabled = true;
                    txtsaldoup.Enabled = true;
                    txtnombreup.Enabled = true;
                    txtapellidosup.Enabled = true;
                    txtcedulaup.Enabled = true;
                    button5.Enabled = true;
                    button6.Enabled = true;
                    cdao.close();
                }
                else {
                    MessageBox.Show(this, "Error No existe esa cuenta");
                    cdao.close();
                }
            }
        }

        private void comboBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            txtnombreup.Enabled = true;
            comboBox2.Enabled = false;
            txtcuentaup.Enabled = true;
            txtpingup.Enabled = false;
            txtsaldoup.Enabled = false;
            txtnombreup.Enabled = false;
            txtapellidosup.Enabled = false;
            txtcedulaup.Enabled = false;
            button5.Enabled = false;
            button6.Enabled = false;
            txtcuentaup.Text = "";
            txtpingup.Text = "";
            txtsaldoup.Text = "";
            txtnombreup.Text = "";
            txtapellidosup.Text = "";
            txtcedulaup.Text = "";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            
            DAO cdao = new DAO("cajero.txt");
            Archivo c = new Archivo();
            Archivo a = new Archivo();
            a = cdao.Findcuenta(int.Parse(txtcuentaup.Text));
            int id = a.Id;
            String nombre= a.Nombre;
            String apellidos= a.Apellido;
            String ced = a.Cedula;
            int ping = a.Pin;
            int cuenta = a.Cuenta;
            Double saldo = a.Saldo;
            Boolean cordoba = a.Cordoba;

            if (txtcedulaup.Text.Equals(""))
            {
                MessageBox.Show("Escriba la cedula");
                txtcedulaup.Focus();
            }
            else {
                ced = txtcedulaup.Text;
            }
            if (txtapellidosup.Text.Equals(""))
            {
                MessageBox.Show("Escriba el Apellido del cliente");
                txtapellidosup.Focus();
            }
            else {
                apellidos=  txtapellidosup.Text;
            }
            if (txtnombreup.Text.Equals(""))
            {
                MessageBox.Show("Escriba el nombre del cliente");
                txtnombreup.Focus();
            }
            else {
                nombre = txtnombreup.Text;
            }
            if (comboBox1.Text.Equals("Dollares"))
             {
                 cordoba = false;
             }
            if (txtsaldoup.Text.Equals(""))
            {
                MessageBox.Show("Escriba el saldo");
                txtsaldoup.Focus();
            }
            else {
                saldo = double.Parse(txtsaldoup.Text);
            }

            if (txtpingup.Text.Equals(""))
            {
                MessageBox.Show("Escriba el ping");
                txtpingup.Focus();
            }
            else {
                ping = int.Parse(txtpingup.Text);            }
            if (txtcuentaup.Text.Equals(""))
            {
                MessageBox.Show("Escriba la nueva cuenta");
                txtcuentaup.Focus();
            }
            else
            {
                
                
                try
                {
                    
                    c.Id = id;
                    c.Nombre = nombre;
                    c.Apellido = apellidos;
                    c.Cedula = ced;
                    c.Cuenta = cuenta;
                    c.Pin = ping;
                    c.Saldo = saldo;
                    c.Cordoba = cordoba;

                    try
                    {
                        cdao.Update(c);
                        cdao.close();
                        this.button5_Click(sender, e);
                    }
                    catch (Exception E)
                    {
                        MessageBox.Show(this, "llene los campos correctamente" + E);
                    }

                }
                catch (Exception E)
                {
                    MessageBox.Show(this, "llene los campos correctamente" + E);
                }

            }

        }

        private void textBox2_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            char c = e.KeyChar;

            if (Char.IsLetter(c) || Char.IsPunctuation(c) || Char.IsSeparator(c) || e.KeyChar == '$' || e.KeyChar == '=' || e.KeyChar == '+' || e.KeyChar == '´')
            {
                e.Handled = true;
            }
            
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            char c = e.KeyChar;

            if (Char.IsLetter(c) || Char.IsPunctuation(c) || Char.IsSeparator(c) || e.KeyChar == '$' || e.KeyChar == '=' || e.KeyChar == '+' || e.KeyChar == '´')
            {
                e.Handled = true;
            }
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                try
                {
                    DAO cdao = new DAO("cajero.txt");
                    Archivo a = new Archivo();

                    int cue = int.Parse(textBox2.Text);
                    int pi = int.Parse(textBox3.Text);
                    a = cdao.Findcuenta(cue);
                    if (a.Cuenta == cue)
                    {
                        int id = a.Id;
                        String nombre = a.Nombre;
                        String apellidos = a.Apellido;
                        String ced = a.Cedula;
                        int ping = a.Pin;
                        int cuenta = int.Parse(textBox2.Text);
                        Double saldo = a.Saldo;
                        Boolean cordoba = a.Cordoba;
                        cdao.close();
                        if (ping == pi)
                        {
                            MessageBox.Show("Bienvenido " + a.Nombre);
                            tabControl2.Enabled = true;
                            button7.Enabled = false;
                            button8.Enabled = true;
                            String mo = "";
                            if (a.Cordoba == true)
                            {
                                mo = "C$ ";
                            }
                            else
                            {
                                mo = "$ ";
                            }

                            textBox5.Text = mo + a.Saldo.ToString();
                            textBox4.Text = mo + a.Saldo.ToString();
                            textBox3.Enabled = false;
                            textBox2.Enabled = false;
                            cdao.close();
                        }
                        else
                        {
                            MessageBox.Show("Error en concidencia de pin");
                        }

                    }
                    else
                    {
                        MessageBox.Show("Cuenta No existente");
                    }
                    cdao.close();
                }
                catch (Exception ert) {
                    MessageBox.Show("Error: Ingrese una Cuenta o pin");
                }

            }
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_KeyPress(object sender, EventArgs e)
        {
           
                try {
                    DAO cdao = new DAO("cajero.txt");
                    Archivo a = new Archivo();
                    int cue = int.Parse(textBox2.Text);
                    int pi = int.Parse(textBox3.Text);
                    a = cdao.Findcuenta(cue);
                    if (a.Cuenta == cue)
                    {
                        int id = a.Id;
                        String nombre = a.Nombre;
                        String apellidos = a.Apellido;
                        String ced = a.Cedula;
                        int ping = a.Pin;
                        int cuenta = int.Parse(textBox2.Text);
                        Double saldo = a.Saldo;
                        Boolean cordoba = a.Cordoba;
                        cdao.close();
                        if (ping == pi)
                        {
                            MessageBox.Show("Bienvenido " + a.Nombre);
                            tabControl2.Enabled = true;
                            button7.Enabled = false;
                            button8.Enabled = true;
                            String mo = "";
                            if (a.Cordoba == true)
                            {
                                mo = "C$ ";
                            }
                            else {
                                mo = "$ ";
                            }

                            textBox5.Text = mo+a.Saldo.ToString();
                            textBox4.Text = mo+a.Saldo.ToString();
                            textBox3.Enabled = false;
                            textBox2.Enabled = false;
                            cdao.close();
                        }
                        else
                        {
                            MessageBox.Show("Error en concidencia de pin");
                        }

                    }
                    else
                    {
                        MessageBox.Show("Cuenta No existente");
                    }
                    cdao.close();
                }
                    catch(Exception er){
                    MessageBox.Show("Error: Ingrese Cuenta o ping");
                    
                    }
            
                

        }

        private void radioButton26_CheckedChanged(object sender, EventArgs e)
        {
            groupBox1.Enabled = true;
            groupBox2.Enabled = false;
            
        }

        private void radioButton25_CheckedChanged(object sender, EventArgs e)
        {
            groupBox2.Enabled = true;
            groupBox1.Enabled = false;
        }

        private void radioButton13_CheckedChanged(object sender, EventArgs e)
        {
            groupBox6.Enabled = true;
            groupBox4.Enabled = false;
        }

        private void radioButton14_CheckedChanged(object sender, EventArgs e)
        {
            groupBox6.Enabled = false;
            groupBox4.Enabled = true;
        }

        private void Retirar(object sender, EventArgs e)
        {
            Archivo a = new Archivo();
            Archivo c = new Archivo();
            DAO cdao = new DAO("cajero.txt");
            a = cdao.Findcuenta(int.Parse(textBox2.Text));
            int id = a.Id;
            String nombre= a.Nombre;
            String apellidos= a.Apellido;
            String ced = a.Cedula;
            int ping = a.Pin;
            int cuenta = a.Cuenta;
            Double saldo = a.Saldo;
            Boolean cordoba = a.Cordoba;
            if (radioButton13.Checked) {
                if (radioButton21.Checked)
                {
                    if (a.Cordoba == true)
                    {
                        double sal = a.Saldo - (1000 * 1.05);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                    else
                    {
                        double sal = (a.Saldo * 35) - (1000 * 1.05);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal / 35;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                }

                if (radioButton22.Checked)
                {
                    if (a.Cordoba == true)
                    {
                        double sal = a.Saldo - (800 * 1.05);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                    else
                    {
                        double sal = (a.Saldo * 35) - (800 * 1.05);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal / 35;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                }

                if (radioButton23.Checked)
                {
                    if (a.Cordoba == true)
                    {
                        double sal = a.Saldo - (600*1.05);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                    else
                    {
                        double sal = (a.Saldo * 35) - (600*1.05);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal / 35;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                }


                if (radioButton24.Checked)
                {
                    if (a.Cordoba == true)
                    {
                        double sal = a.Saldo - (500*1.05);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                    else
                    {
                        double sal = (a.Saldo * 35) - (500*1.05);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal / 35;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                }

                if (radioButton27.Checked)
                {
                    if (a.Cordoba == true)
                    {
                        double sal = a.Saldo - (200*1.05);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                    else
                    {
                        double sal = (a.Saldo * 35) - (200*1.05);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal / 35;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                }


                if (radioButton28.Checked)
                {
                    if (a.Cordoba == true)
                    {
                        double sal = a.Saldo - (100*1.05);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                    else
                    {
                        double sal = (a.Saldo * 35) - (100*1.05);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal / 35;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                }

            }
            if (radioButton14.Checked) {
                if (radioButton15.Checked)
                {
                    if (a.Cordoba == false)
                    {
                        double sal = a.Saldo - (100 * 1.05);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                    else
                    {
                        double sal = a.Saldo - (100 * 35);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                }
                if (radioButton16.Checked)
                {
                    if (a.Cordoba == false)
                    {
                        double sal = a.Saldo - (80 * 1.05);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                    else
                    {
                        double sal = a.Saldo - (80 * 24);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                }
                if (radioButton17.Checked)
                {
                    if (a.Cordoba == false)
                    {
                        double sal = a.Saldo - (60 * 1.05);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                    else
                    {
                        double sal = a.Saldo - (60 * 24);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                }

                if (radioButton18.Checked)
                {
                    if (a.Cordoba == false)
                    {
                        double sal = a.Saldo - (50 * 1.05);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                    else
                    {
                        double sal = a.Saldo - (50 * 35);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                }

                if (radioButton19.Checked)
                {
                    if (a.Cordoba == false)
                    {
                        double sal = a.Saldo - (20 * 1.05);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                    else
                    {
                        double sal = a.Saldo - (20 * 35);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                }

                if (radioButton20.Checked)
                {
                    if (a.Cordoba == false)
                    {
                        double sal = a.Saldo - (10*1.05);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                    else
                    {
                        double sal = a.Saldo - (10 * 35);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                }
            }
            textBox5.Text = "";
            String mo = "";
            if (c.Cordoba == true)
            {
                mo = "C$ ";
            }
            else
            {
                mo = "$ ";
            }
            MessageBox.Show("Cliente: " + a.Cuenta + "\n Retiro es: "+mo+(a.Saldo-c.Saldo)+"\n Su Nuevo saldo es: " + mo + c.Saldo);
            cdao.close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            Archivo a = new Archivo();
            Archivo c = new Archivo();
            DAO cdao = new DAO("cajero.txt");
            a = cdao.Findcuenta(int.Parse(textBox2.Text));
            int id = a.Id;
            String nombre= a.Nombre;
            String apellidos= a.Apellido;
            String ced = a.Cedula;
            int ping = a.Pin;
            int cuenta = a.Cuenta;
            Double saldo = a.Saldo;
            Boolean cordoba = a.Cordoba;
            String tt = "";
            if(radioButton26.Checked){
                tt = "C$";
                if (radioButton6.Checked)
                {
                    if (a.Cordoba == true)
                    {
                        double sal = a.Saldo - 1000;
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                    else
                    {
                        double sal = (a.Saldo * 24) - 1000;
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal / 24;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                }

                if (radioButton5.Checked)
                {
                    if (a.Cordoba == true)
                    {
                        double sal = a.Saldo - 800;
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                    else
                    {
                        double sal = (a.Saldo * 24) - 800;
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal / 24;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                }
                if (radioButton4.Checked)
                {
                    if (a.Cordoba == true)
                    {
                        double sal = a.Saldo - 600;
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                    else
                    {
                        double sal = (a.Saldo * 24) - 600;
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal / 24;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                }

                if (radioButton3.Checked)
                {
                    if (a.Cordoba == true)
                    {
                        double sal = a.Saldo - 500;
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                    else
                    {
                        double sal = (a.Saldo * 24) - 500;
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal / 24;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                }

                if (radioButton2.Checked)
                {
                    if (a.Cordoba == true)
                    {
                        double sal = a.Saldo - 200;
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                    else
                    {
                        double sal = (a.Saldo * 24) - 200;
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;                                
                                c.Saldo = sal/24;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                }

            
                if(radioButton1.Checked){
                    if (a.Cordoba == true) 
                    {
                        double sal = a.Saldo-100;
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                    else {
                        double sal = (a.Saldo * 24) - 100;
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal/24;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }
                        
                    }
                }

            }
            if (radioButton25.Checked)
            {
                tt = "$ ";
                if (radioButton7.Checked)
                {
                    if (a.Cordoba == false)
                    {
                        double sal = a.Saldo - 100;
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                    else
                    {
                        double sal = a.Saldo - (100 * 24);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                }
                if (radioButton8.Checked)
                {
                    if (a.Cordoba == false)
                    {
                        double sal = a.Saldo - 80;
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                    else
                    {
                        double sal = a.Saldo - (80 * 24);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                }
                if (radioButton9.Checked)
                {
                    if (a.Cordoba == false)
                    {
                        double sal = a.Saldo - 60;
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                    else
                    {
                        double sal = a.Saldo - (60 * 24);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                }
                if (radioButton10.Checked)
                {
                    if (a.Cordoba == false)
                    {
                        double sal = a.Saldo - 50;
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                    else
                    {
                        double sal = a.Saldo - (50 * 24);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                }
                if (radioButton11.Checked)
                {
                    if (a.Cordoba == false)
                    {
                        double sal = a.Saldo - 20;
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                    else
                    {
                        double sal = a.Saldo - (20 * 24);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                }
                if (radioButton12.Checked)
                {
                    if (a.Cordoba == false)
                    {
                        double sal = a.Saldo - 10;
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                    else
                    {
                        double sal = a.Saldo - (10*24);
                        if (sal < 0)
                        {
                            c.Saldo = a.Saldo;
                            MessageBox.Show("Estimado usuario no requiere los fondos necesario para la Transicion");
                        }
                        else
                        {
                            try
                            {

                                c.Id = id;
                                c.Nombre = nombre;
                                c.Apellido = apellidos;
                                c.Cedula = ced;
                                c.Cuenta = cuenta;
                                c.Pin = ping;
                                c.Saldo = sal;
                                c.Cordoba = cordoba;

                                try
                                {
                                    cdao.Update(c);
                                    cdao.close();
                                    this.button8_Click(sender, e);
                                }
                                catch (Exception E)
                                {
                                    MessageBox.Show(this, "Error:" + E);
                                }

                            }
                            catch (Exception E)
                            {
                                MessageBox.Show(this, "Error:" + E);
                            }
                        }

                    }
                }
            }
            String mo = "";
            if (c.Cordoba == true)
            {
                mo = "C$ ";
            }
            else {
                mo = "$ ";
            }
            MessageBox.Show("Cliente: " + a.Cuenta + "\n Retiro es: " +mo+ (a.Saldo - c.Saldo) + "\n Su Nuevo saldo es: " + mo + c.Saldo);
            cdao.close();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            textBox3.Enabled = true;
            textBox2.Enabled = true;
            textBox2.Text = "";
            textBox3.Text = "";
            tabControl2.Enabled = false;
            button8.Enabled = false;
            textBox4.Text = "";
            button7.Enabled = true;
            textBox2.Focus();
        }

        private void txtpingup_TextChanged(object sender, EventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {
            Archivo a = new Archivo();
            DAO cdao = new DAO("cajero.txt");
            table.Rows.Clear();
            

            for (int i = 1; i <= cdao.tamar(); i++)
            {
                a = cdao.Findid(i);
                String moneda = "";
                String mon = "";
                if (a.Cordoba == true)
                {
                    mon = "C$ ";
                    moneda = "Cordobas";
                }
                else
                {
                    mon = "$ ";
                    moneda = "Dollares";
                }
                table.Rows.Add(a.Cuenta, a.Nombre + " " + a.Apellido, a.Cedula, moneda, mon + a.Saldo);
            }
            cdao.close();
        }

        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
             char c = e.KeyChar;

            if (Char.IsLetter(c) || Char.IsPunctuation(c) || Char.IsSeparator(c) || e.KeyChar == '$' || e.KeyChar == '=' || e.KeyChar == '+' || e.KeyChar == '´')
            {
                e.Handled = true;
            }
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                try {
                    DAO cdao = new DAO("cajero.txt");
                    Archivo a = new Archivo();

                    int cue = int.Parse(textBox6.Text);
                    a = cdao.Findcuenta(cue);
                    if (a.Cuenta == cue)
                    {

                        Double saldo = a.Saldo;
                        Boolean cordoba = a.Cordoba;

                        if (cordoba == true)
                        {
                            textBox7.Text = "Cordobas";
                            textBox8.Text = "Dollares";
                            textBox9.Text = saldo.ToString();
                            textBox10.Text = (saldo / 24).ToString();
                        }
                        else
                        {
                            textBox8.Text = "Cordobas";
                            textBox7.Text = "Dollares";
                            textBox9.Text = saldo.ToString();
                            textBox10.Text = (saldo * 24).ToString();
                        }
                        textBox6.Enabled = false;
                        button12.Enabled = true;
                        button13.Enabled = true;

                    }
                    else
                    {
                        MessageBox.Show("Error: No existe Cuenta");
                    } cdao.close();
                


                }
                catch(Exception ee){
                    MessageBox.Show("Error: "+e);
                }
            }
        }

        private void tabPage8_Click(object sender, EventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox10_TextChanged(object sender, EventArgs e)
        {

        }

        private void button13_Click(object sender, EventArgs e)
        {
            textBox6.Text = "";
            textBox7.Text = "";
            textBox8.Text = "";
            textBox9.Text = "";
            textBox10.Text = "";
            textBox6.Enabled = true;
            button12.Enabled = false;
            button13.Enabled = false;

        }

        private void button12_Click(object sender, EventArgs e)
        {
            Archivo a = new Archivo();
            DAO cdao = new DAO("cajero.txt");
            Archivo b = new Archivo();

            a = cdao.Findcuenta(int.Parse(textBox6.Text));
            b.Id = a.Id;
            b.Nombre = a.Nombre;
            b.Apellido = a.Apellido;
            b.Cedula = a.Cedula;
            b.Cuenta = a.Cuenta;
            b.Pin = a.Pin;
            b.Saldo = int.Parse(textBox10.Text);
            if (a.Cordoba == true)
            {
                b.Cordoba = false;
            }
            else {
                b.Cordoba = true;
            }
            try
            {
                cdao.Update(b);
                cdao.close();
            }
            catch(Exception ee) {
                MessageBox.Show("Error: "+e);
                cdao.close();
            }

            this.button13_Click(sender,e);
        }


                
      
    }
}
