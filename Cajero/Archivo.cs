﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Cajero
{
    class Archivo
    {
        private int id;
          private string nombre;
          private string apellido;
          private string cedula;
          private int cuenta;
          private int pin;
          private double saldo;
          private Boolean cordoba;
         


        public Archivo() { }

        public Archivo(int id,string nombre,string apellido,string cedula,int cuenta,int pin,Boolean cordoba, double saldo)
        {
            Id = id;
            Nombre = nombre;
            Apellido = apellido;
            Cedula = cedula;
            Cuenta = cuenta;
            Cordoba = cordoba;
            Pin = pin;
            Saldo = saldo;

        }

        public int Id
        {
            get { return id;}
            set { id = value; }
        }
        

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }
        public string Cedula
        {
            get { return cedula; }
            set { cedula = value; }
        }
        public int Cuenta
        {
            get { return cuenta;}
            set { cuenta = value; }
        }
        public Boolean Cordoba
        {
            get { return cordoba; }
            set { cordoba = value; }
        }
        public int Pin
        {
            get { return pin; }
            set { pin = value; }
        }

        public double Saldo
        {
            get { return saldo; }
            set { saldo = value; }
        }

          }
}
